FROM openjdk:11
EXPOSE 8787:8787
RUN rm -rf ./appbuild
RUN mkdir /appbuild
COPY . /appbuild

WORKDIR /appbuild/
RUN ./gradlew clean build

WORKDIR /appbuild/libs
CMD ["java authorization-0.0.1.jar MONGO_URI=mongodb+srv://dbUser:qwe321321@cluster0.wc5q1.mongodb.net/Account?retryWrites=true&w=majority PORT=8787"]