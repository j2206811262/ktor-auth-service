package bean

import org.bson.codecs.pojo.annotations.BsonId

data class UserInfo (
    val templeId: String,
    val phoneNumber: String,
    @BsonId val id: String? = null,
    val account: String
)

data class SendSMSRequest (val templeId: String?, val phoneNumber: String, val templeName: String?, @BsonId val id: String? = null)

data class VerifyCodeRequest(
    val templeId: String?,
    val phoneNumber: String,
    val verificationCode: String?,
    @BsonId val id: String? = null
)