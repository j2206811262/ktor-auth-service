package com.authorization.bean

class MyTokenInfo(val token: String, val refreshToken: String, val uniqueId: String)