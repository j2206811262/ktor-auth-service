package com.authorization.model.error

import io.ktor.client.features.*
import io.ktor.client.statement.*
import kotlinx.serialization.Serializable


@Serializable
data class Error(val code: Int, val message: String)

class CustomResponseException(response: HttpResponse, cachedResponseText: String) :
    ResponseException(response, cachedResponseText) {
    override val message: String = "Custom server error: ${response.call.request.url}. " +
            "Status: ${response.status}. Text: \"$cachedResponseText\""
}
