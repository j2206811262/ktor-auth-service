package bean

import io.ktor.http.*
import kotlinx.serialization.Serializable

@Serializable
data class BaseResponse(val message: String)