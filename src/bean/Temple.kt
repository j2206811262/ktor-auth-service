package com.authorization.bean

import kotlinx.serialization.Serializable

@Serializable
data class Temple(
    val templeName: String,
    val templeId: String
)
