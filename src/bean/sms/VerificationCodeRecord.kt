package com.authorization.model.sms

import org.bson.codecs.pojo.annotations.BsonId
import java.util.*

data class VerificationCodeRecord(val phoneNumber: String, val verificationCode: String, val createdTime: Date, @BsonId val id : String? = null)