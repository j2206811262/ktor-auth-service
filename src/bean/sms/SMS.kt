package com.authorization.model.sms

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class SMS(
    @SerializedName("UID")
    val senderID: String = "",
    @SerializedName("PWD")
    val senderPassword: String = "",
    @SerializedName("SB")
    val subject: String = "",
    @SerializedName("MSG")
    val message: String = "",
    @SerializedName("RETRYTIME")
    val retryTime: Int = 0,
    @SerializedName("Name")
    val receiverName: String = "",
    @SerializedName("DEST")
    val phoneNumber: String = "",
    @SerializedName("Email")
    val email: String = "",
    @SerializedName("SendTime")
    val sendTime: String = "",
    @SerializedName("Param")
    val param: String = "",
    @SerializedName("MR")
    val messageId: String = ""
)

//參數說明 (HTTP Body, Json format)
//參數名稱 說明
//UID 帳號(必)。
//PWD 密碼(必)。
//SB 簡訊主旨，預設為空字串。主旨不會隨著簡訊內容發送，用以註記本次發送之用途。
//MSG
// 若發送內容參數化時，參數以%field1%、%field2%、…、%field5%帶入（最
//多 5 組）。
// 若為個人化簡訊，則為空字串。
//RETRYTIME 簡訊有效期限(單位：分鐘)，預設為 1440 分鐘。
//Name 收訊人名稱，預設為空字串。若帶入姓名，將顯示於發送紀錄中，以便識別。
//Mobile 接收人之手機號碼(必)。格式：+886912345678 或 0912345678。
//Email
//收訊人電子郵件，預設為空字串。EVERY8D 將簡訊內容一同傳送郵件給收訊人，
//此部份不收費。
//SendTime
//預設為空字串。若預約不同發送時間，於此設定。格式：yyyyMMddHHmmss，
//預設為空字串。例如:預約 2999/01/31 15:30:00 發送，則傳入 29990131153000。
//Param
//(必)
// 若發送參數簡訊，|為分隔符號。系統支援五組參數，接受空字串，且分隔符號
//不可省略，如: ABCDE|888|2999-01-31||。
//EVERY8D 企業簡訊平台 HTTPAPI-2.1 規格
//本規格文件由互動資通股份有限公司授權使用 Page 16 of 31
//Copyright © 2021 by EVERY8D, All Rights Reserved.
//參數名稱 說明
//範例：
// MSG：
//親愛的客戶%field1%，您好:
//你的帳號%field2%將於%field3%到期。
// Param：
//Eric|888888|2009-01-31||
// 實際內容為:
//親愛的客戶 Eric，您好:
//你的帳號 888888 將於 2009-01-31 到期。
// Param：
//|888888|2009-01-31||
// 實際內容為:
//親愛的客戶，您好:
//你的帳號 888888 將於 2009-01-31 到期。
// 若發送個人化簡訊則為發送內容。
//MR
//每筆簡訊的發送代碼，預設為空字串。此代碼於同批發送資料必須是唯一值；若於
//發送時帶入，則狀態回報連同此代碼一同回傳。
