package com.authorization.bean

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId

@Serializable
data class TempleInfo(
    @BsonId
    var _id: String?,
    @SerializedName("ref_id")
    var refId: String?,
    val name: String,
    val owner: String?,
    val address: String?,
    val phone: String?,
    val mobile: String?,
    @SerializedName("opening_hour_open")
    val openingHourOpen: String?,
    @SerializedName("opening_hour_close")
    val openingHourClose: String?,
    @SerializedName("bank_account_info")
    val bankAccountInfo: BankAccountInfo?,
    var revision: Int
) {
    @Serializable
    data class BankAccountInfo(
        @SerializedName("bank_code")
        val bankCode: String?,
        @SerializedName("bank_name")
        val bankName: String?,
        @SerializedName("account_number")
        val accountNumber: String?,
        @SerializedName("account_owner")
        val accountOwner: String?
    )
}