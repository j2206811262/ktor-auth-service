package com.authorization.utilities

import com.auth0.jwt.interfaces.Payload
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter.ofPattern

object Utils {
    fun getCurrentDateInUTCFormat(): String {
        return ofPattern("yyyy-MM-dd")
            .withZone(ZoneOffset.UTC)
            .format(Instant.now())
    }
}