package com.authorization.constant

object Constants {
    const val DB_NAME = "antaisui"
    const val COLLECTION_NAME_TEMPLE_INFO = "temple_info"
    const val COLLECTION_NAME_TEMPLE_ACCOUNT_INFO = "temple_account_info"

    const val REF_ID = "ref_id"
    const val NAME = "name"

    const val TOKEN = "token"
    const val REFRESH_TOKEN = "refresh_token"
    const val TOKEN_TYPE = "token_type"
    const val UNIQUE_ID = "unique_id"
    const val USER_ID = "user_id"
    const val ROLE = "role"
    const val TEMPLE_ID = "temple_id"
    const val SCOPES = "scopes"

    const val SCOPE_REGISTRATION = "registration"
    const val SCOPE_CANDLE_TYPE = "candle_type"
    const val SCOPE_CATEGORY = "category"
    const val REFRESH_TOKEN_EXPIRE_TIME_SECONDS: Long = 3600 * 24 * 3 //3days
    const val VERIFICATION_CODE_EXPIRE_TIME: Long = 300
}