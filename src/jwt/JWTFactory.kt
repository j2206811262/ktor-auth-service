package com.authorization.jwt

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.authorization.bean.MyTokenInfo
import com.authorization.constant.Constants
import com.authorization.utilities.Utils
import com.fasterxml.uuid.Generators
import org.koin.core.logger.PrintLogger
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*


object JWTFactory {
    private val currentDate = Utils.getCurrentDateInUTCFormat()
    private val secretKey = "antaisui-$currentDate"
    private val encodedSecretKey =  Base64.getEncoder().encodeToString(secretKey.toByteArray())
    private val algorithm = Algorithm.HMAC256(encodedSecretKey)
    val verifier: JWTVerifier = JWT.require(algorithm).build()
    private val utcNow = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

    fun sign(userId: String, isAdmin: Boolean, templeId: String): MyTokenInfo {
        val uniqueId = Generators.timeBasedGenerator().generate().toString()
        val issueAt = utcNow.time
        val expiredDate =  utcNow.apply {
            this.add(Calendar.MINUTE, 60)
        }.time
        val expiredRefreshTokenDate = utcNow.apply {
            this.add(Calendar.DAY_OF_YEAR, 7)
        }.time

        PrintLogger().info("secretKey: $secretKey")
        PrintLogger().info("Expired (exp): $expiredDate")
        PrintLogger().info("Issuer(iss): $userId")
        PrintLogger().info("Issuer(iat): $issueAt")

        val accessToken = JWT.create()
            .withIssuer(userId)
            .withIssuedAt(issueAt)
            .withExpiresAt(expiredDate)
            .withClaim(Constants.USER_ID, userId)
            .withClaim(Constants.UNIQUE_ID, uniqueId)
            .withClaim(Constants.ROLE, if (isAdmin) "admin" else "user")
            .withClaim(Constants.TEMPLE_ID, templeId)
            .withClaim(Constants.SCOPES, generateScopeByRole(isAdmin))
            .sign(algorithm)

        val refreshToken = JWT.create()
            .withIssuer(userId)
            .withIssuedAt(issueAt)
            .withExpiresAt(expiredRefreshTokenDate)
            .withClaim(Constants.USER_ID, userId)
            .withClaim(Constants.UNIQUE_ID, uniqueId)
            .withClaim(Constants.ROLE, if (isAdmin) "admin" else "user")
            .withClaim(Constants.TEMPLE_ID, templeId)
            .withClaim(Constants.SCOPES, generateScopeByRole(isAdmin))
            .sign(algorithm)

        return MyTokenInfo(accessToken, refreshToken, uniqueId)
    }

    private fun generateScopeByRole(isAdmin: Boolean): List<String> {
        return if (isAdmin) {
            listOf(Constants.SCOPE_REGISTRATION, Constants.SCOPE_CANDLE_TYPE, Constants.SCOPE_CATEGORY)
        } else {
            listOf(Constants.SCOPE_REGISTRATION, Constants.SCOPE_CANDLE_TYPE)
        }
    }
}