package com.authorization

import bean.BaseResponse
import com.authorization.controller.AdminLoginController
import com.authorization.controller.RefreshController
import com.authorization.controller.SMSServiceController
import com.authorization.controller.UserInfoController
import com.authorization.jwt.JWTFactory
import com.authorization.utilities.appModule
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.core.context.startKoin
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
fun Application.module() {
    startKoin {
        modules(appModule)
    }
    val mongoDBClient = KMongo.createClient(environment.config.property("ktor.mongoUri").getString()).coroutine

    install(CORS) {
        anyHost()

        header(HttpHeaders.ContentType)
        header(HttpHeaders.Authorization)

        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost()
    }

    install(Authentication) {
        jwt("auth-jwt") {
            verifier(JWTFactory.verifier)
            validate { jwtCredential ->
                JWTPrincipal(jwtCredential.payload)
            }
        }
    }

    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }

    install(Locations)

    routing {
        SMSServiceController(mongoDBClient)

        UserInfoController(mongoDBClient)

        AdminLoginController(mongoDBClient)

        RefreshController()

        get("/health") {
            call.respond(BaseResponse("OK"))
        }
    }
}

