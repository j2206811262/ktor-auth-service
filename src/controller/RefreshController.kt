package com.authorization.controller

import bean.BaseResponse
import com.authorization.constant.Constants
import com.authorization.jwt.JWTFactory
import com.authorization.redis.RedisManager
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import redis.clients.jedis.Jedis


fun Route.RefreshController() {

    authenticate("auth-jwt") {
        post("/refresh") {
            val principal = call.principal<JWTPrincipal>()
            if (principal == null) {
                call.respond(HttpStatusCode.BadRequest, BaseResponse("token error"))
            } else {
                val authHeader = call.request.headers["Authorization"]
                val phoneNumber = principal.issuer
                val templeId = principal.payload.getClaim(Constants.TEMPLE_ID).asString()
                val isAdmin = principal.payload.getClaim(Constants.ROLE).asString() == "admin"

                if (authHeader != null && authHeader.split(" ").size == 2
                    && phoneNumber != null
                ) {
                    val token = authHeader.split(" ")[1]
                    val jedis = Jedis(RedisManager.REDIS_HOST, RedisManager.REDIS_PORT)

                    if (RedisManager.hasKeyOnRedis(token)) {
                        RedisManager.deleteByKey(token)

                        val myToken = JWTFactory.sign(phoneNumber, isAdmin, templeId)
                        RedisManager.addKeyToRedis(
                            myToken.refreshToken,
                            "aloha",
                            Constants.REFRESH_TOKEN_EXPIRE_TIME_SECONDS
                        )

                        call.respond(
                            HttpStatusCode.OK, mapOf(
                                Constants.TOKEN to myToken.token,
                                Constants.REFRESH_TOKEN to myToken.refreshToken,
                                Constants.TOKEN_TYPE to "bearer"
                            )
                        )
                    }
                    jedis.close()
                }

            }
        }
    }
}
