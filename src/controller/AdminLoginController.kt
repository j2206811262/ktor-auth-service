package com.authorization.controller

import bean.BaseResponse
import bean.UserInfo
import com.authorization.constant.Constants
import com.google.gson.Gson
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject
import org.litote.kmongo.and
import org.litote.kmongo.coroutine.CoroutineClient
import org.litote.kmongo.eq

fun Route.AdminLoginController(mongoDBClient: CoroutineClient) {
    fun clientAccountInfo() = mongoDBClient.getDatabase(Constants.DB_NAME)
        .getCollection<UserInfo>(Constants.COLLECTION_NAME_TEMPLE_ACCOUNT_INFO)

    suspend fun parseLoginRegister(call: ApplicationCall): UserInfo {
        val post = call.receive<UserInfo>()
        val gson: Gson by inject()
        return UserInfo(
            templeId = gson.fromJson(post.templeId, String::class.java),
            phoneNumber = gson.fromJson(post.phoneNumber, String::class.java),
            account = gson.fromJson(post.account, String::class.java)
        )
    }

    post("/sign-up/AndersonIsYourBoss") {
        val loginRegister = parseLoginRegister(call)
        clientAccountInfo().findOne(
            and(
                UserInfo::templeId eq loginRegister.templeId,
                UserInfo::phoneNumber eq loginRegister.phoneNumber,
                UserInfo::account eq  loginRegister.account
            )
        )?.let {
            call.respond(HttpStatusCode.BadRequest, BaseResponse("This user name is already exist!"))
        }

        clientAccountInfo().insertOne(loginRegister)
        call.respond(
            HttpStatusCode.Created, loginRegister
        )

    }
}