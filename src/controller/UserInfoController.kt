package com.authorization.controller

import bean.BaseResponse
import com.authorization.bean.TempleInfo
import com.authorization.constant.Constants
import com.fasterxml.uuid.Generators
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.bson.Document
import org.litote.kmongo.and
import org.litote.kmongo.coroutine.CoroutineClient
import org.litote.kmongo.descending
import org.litote.kmongo.eq
import org.litote.kmongo.ne

fun Route.UserInfoController(mongoDBClient: CoroutineClient) {

    fun clientLoginRegister() = mongoDBClient.getDatabase(Constants.DB_NAME)
        .getCollection<TempleInfo>(Constants.COLLECTION_NAME_TEMPLE_INFO)

    suspend fun addToTempleInfo(templeInfo: TempleInfo): Boolean {
        val result = clientLoginRegister().insertOne(templeInfo).wasAcknowledged()
        return result
    }

    post("/createTempleDataTest") {
        val templeInfo = TempleInfo(
            _id = Generators.timeBasedGenerator().generate().toString(),
            name = "楊梅玉玄宮",
            refId = Generators.timeBasedGenerator().generate().toString(),
            owner = "陳建成",
            address = "桃園市楊梅區什麼路又什麼號",
            phone = "03-123456",
            mobile = "0982711806",
            openingHourOpen = "09:00",
            openingHourClose = "18:00",
            bankAccountInfo = TempleInfo.BankAccountInfo(
                bankCode = "012",
                bankName = "富邦銀行",
                accountNumber = "420168274806",
                accountOwner = "陳建成"
            ),
            revision = 1
        )

        val queryTempleInfo = clientLoginRegister()
            .find(TempleInfo::name eq templeInfo.name)
            .sort(Document(TempleInfo::revision.name, -1))
            .limit(1)
            .first()

        if (queryTempleInfo != null) {
            templeInfo.revision = queryTempleInfo.revision + 1
        }

        call.respond(
            BaseResponse(
                clientLoginRegister().insertOne(templeInfo).toString()
            )
        )
    }

    post("/createTemple") {
        val templeInfo = call.receive<TempleInfo>()

        val queryTempleInfo = clientLoginRegister().findOne(TempleInfo::name eq templeInfo.name)

        if (queryTempleInfo == null) {
            templeInfo._id = Generators.timeBasedGenerator().generate().toString()
            if (templeInfo.refId.isNullOrEmpty()) {
                templeInfo.refId = Generators.timeBasedGenerator().generate().toString()
            }

            val result = addToTempleInfo(templeInfo)
            if (result) {
                call.respond(
                    HttpStatusCode.Created,
                    templeInfo
                )
            } else {
                call.respond(
                    HttpStatusCode.NotFound
                )
            }
        } else {
            call.respond(
                HttpStatusCode.Conflict,
                BaseResponse("record existed!")
            )
        }
    }

    put("/updateTemple") {
        val templeInfo = call.receive<TempleInfo>()

        val queryTempleInfo = clientLoginRegister().findOne(TempleInfo::name eq templeInfo.name)

        if (queryTempleInfo == null) {
            call.respond(
                HttpStatusCode.NotFound
            )
        } else {
            templeInfo._id = Generators.timeBasedGenerator().generate().toString()
            //should not overwrite
            templeInfo.refId = queryTempleInfo.refId

            templeInfo.revision = queryTempleInfo.revision + 1
            clientLoginRegister().insertOne(templeInfo).toString()
            call.respond(
                HttpStatusCode.OK,
                templeInfo
            )
        }
    }

    get("/findTemple") {
        val allParam = call.parameters
        val refId = allParam[Constants.REF_ID]
        val name = allParam[Constants.NAME]
        val condition1 = if (refId == null) {
            TempleInfo::refId ne ""
        } else {
            TempleInfo::refId eq refId.toString()
        }
        val condition2 = if (name == null) {
            TempleInfo::name ne ""
        } else {
            TempleInfo::name eq name.toString()
        }

        val queryTempleInfo: List<TempleInfo> = if (refId != null || name != null) {
            clientLoginRegister()
                .find(and(condition1, condition2))
                .sort(descending(TempleInfo::revision))
                .limit(1)
                .toList()
        } else {
            clientLoginRegister()
                .find()
                .sort(descending(TempleInfo::revision))
                .toList()
        }
        call.respond(
            HttpStatusCode.OK,
            queryTempleInfo
        )
    }


    delete("/deleteTemple") {
        val allParam = call.parameters
        val refId = allParam[Constants.REF_ID].toString()

        val result = clientLoginRegister().deleteMany(TempleInfo::refId eq refId).deletedCount
        if (result > 0) {
            call.respond(
                HttpStatusCode.OK,
                BaseResponse(
                    refId
                )
            )
        } else {
            call.respond(
                HttpStatusCode.NotFound
            )
        }
    }
}
