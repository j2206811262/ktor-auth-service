package com.authorization.controller

import bean.BaseResponse
import bean.SendSMSRequest
import bean.UserInfo
import bean.VerifyCodeRequest
import com.authorization.bean.MyTokenInfo
import com.authorization.constant.Constants
import com.authorization.constant.Constants.REFRESH_TOKEN
import com.authorization.constant.Constants.TOKEN
import com.authorization.constant.Constants.TOKEN_TYPE
import com.authorization.jwt.JWTFactory
import com.authorization.model.sms.SMS
import com.authorization.redis.RedisManager
import com.google.gson.Gson
import io.ktor.application.*
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.koin.core.logger.PrintLogger
import org.koin.ktor.ext.inject
import org.litote.kmongo.and
import org.litote.kmongo.coroutine.CoroutineClient
import org.litote.kmongo.eq

fun Route.SMSServiceController(mongoDBClient: CoroutineClient) {
    fun clientAccountInfo() = mongoDBClient.getDatabase(Constants.DB_NAME)
        .getCollection<UserInfo>(Constants.COLLECTION_NAME_TEMPLE_ACCOUNT_INFO)

    val sendSMSUrl = "http://api.every8d.com/API21/HTTP/sendSMS.ashx"
    val messageTemplate = "您好，您的簡訊驗證碼為: {0} \n , 祝您平安順心!"
    val adminMessageTemplate = "您好管理員，您的簡訊驗證碼為: {0} \n , 感謝您使用本系統!"
    val client = HttpClient(CIO) {
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.ALL
        }
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
    }

    fun thisPhoneNumberIsInLockMode(phoneNumber: String, remoteHost: String): Boolean {
        val phoneNumberAndRemoteHostName = "$phoneNumber-$remoteHost"
        val record = RedisManager.getValue(phoneNumberAndRemoteHostName)
        RedisManager.addKeyToRedis(phoneNumberAndRemoteHostName, "1", 10)
        PrintLogger().info("set phoneNumber: $phoneNumber locked in 10 secs...")
        return (!record.isNullOrEmpty())
    }

    fun isVerifyCodeExistInCache(phoneNumber: String?, verificationCode: String?): Boolean {
        phoneNumber?.let {
            verificationCode?.let {
                return RedisManager.checkVerificationCode(phoneNumber, verificationCode)
            }
        }
        return false
    }

    fun randomGenerateVerificationCode(digit: Int): String {
        var result = ""
        for (i in 0 until digit) {
            result += (0..9).random()
        }
        return result
    }

    fun generateVerificationCode(phoneNumber: String): String {
        var verificationCode = RedisManager.getValue(phoneNumber)
        if (verificationCode.isNullOrEmpty()) {
            verificationCode = randomGenerateVerificationCode(6)
            RedisManager.addKeyToRedis(phoneNumber, verificationCode, Constants.VERIFICATION_CODE_EXPIRE_TIME)
        }
        return verificationCode
    }

    suspend fun parseSendSMSRequest(call: ApplicationCall): SendSMSRequest {
        val post = call.receive<SendSMSRequest>()
        val gson: Gson by inject()
        return SendSMSRequest(
            gson.fromJson(post.templeId, String::class.java),
            gson.fromJson(post.phoneNumber, String::class.java),
            gson.fromJson(post.templeName, String::class.java)
        )
    }

    suspend fun parseVerifyCodeRequest(call: ApplicationCall): VerifyCodeRequest {
        val post = call.receive<VerifyCodeRequest>()
        val gson: Gson by inject()
        return VerifyCodeRequest(
            gson.fromJson(post.templeId, String::class.java),
            gson.fromJson(post.phoneNumber, String::class.java),
            gson.fromJson(post.verificationCode, String::class.java)
        )
    }

    suspend fun sendSMSProcess(phoneNumber: String, message: String): String {
        val response: HttpResponse = client.post(sendSMSUrl) {
            val sms = SMS(
                senderID = "j2206811262",
                senderPassword = "opCode0722",
                subject = "驗證確認",
                message = message,
                phoneNumber = phoneNumber,
                email = "j2206811262@gmail.com"
            )
            body = FormDataContent(Parameters.build {
                append("UID", sms.senderID)
                append("PWD", sms.senderPassword)
                append("SB", sms.subject)
                append("MSG", sms.message)
                append("DEST", sms.phoneNumber)
                append("Email", sms.email)
            })
        }
        return response.receive()

    }

    post("/sendSMS") {
        val sendSMSRequest = parseSendSMSRequest(call)

        if (sendSMSRequest.phoneNumber.isEmpty()) {
            call.respond(HttpStatusCode.BadRequest, BaseResponse("Phone number is not allowed."))
        } else if (thisPhoneNumberIsInLockMode(sendSMSRequest.phoneNumber, call.request.origin.remoteHost)) {
            call.respond(HttpStatusCode.TooManyRequests, BaseResponse("Repeating request received."))
        } else {
            //若此電話存在於DB，則判斷為管理者
            //管理者使用回傳的簡訊內容不同
            val adminAccount = clientAccountInfo().findOne(
                and(
                    SendSMSRequest::templeId eq sendSMSRequest.templeId,
                    SendSMSRequest::phoneNumber eq sendSMSRequest.phoneNumber
                )
            )
            val verificationCode = generateVerificationCode(sendSMSRequest.phoneNumber)
            val message = if (adminAccount == null) {
                messageTemplate
                    .replace("{0}", verificationCode)
            } else {
                adminMessageTemplate
                    .replace("{0}", verificationCode)
            }
            sendSMSProcess(sendSMSRequest.phoneNumber, message)
            call.respond(HttpStatusCode.OK, BaseResponse("send successfully"))
        }
    }

    post("/verifyCode") {
        val verifyCodeRequest = parseVerifyCodeRequest(call)
        if (verifyCodeRequest.templeId.isNullOrEmpty()
            || verifyCodeRequest.phoneNumber.isNullOrEmpty()
            || verifyCodeRequest.verificationCode.isNullOrEmpty()
        ) {
            call.respond(
                HttpStatusCode.BadRequest,
                BaseResponse(
                    "templeId, phoneNumber, verificationCode are required."
                )
            )
            return@post
        }

        val verifyCodeIsExist = isVerifyCodeExistInCache(
            verifyCodeRequest.phoneNumber,
            verifyCodeRequest.verificationCode
        )
        if (verifyCodeIsExist) {
            var myToken: MyTokenInfo? = null
            val keyForTempToken = generateKeyForTempToken(verifyCodeRequest.templeId, verifyCodeRequest.phoneNumber)
            if (RedisManager.hasKeyOnRedis(keyForTempToken)
            ) {
                myToken = Gson().fromJson(
                    RedisManager.getValue(keyForTempToken), MyTokenInfo::class.java
                )
            } else {
                //驗證碼通過
                val adminAccount = clientAccountInfo().findOne(
                    and(
                        UserInfo::templeId eq verifyCodeRequest.templeId,
                        UserInfo::phoneNumber eq verifyCodeRequest.phoneNumber
                    )
                )

                //簽署一個JWT，並且加入refresh token in redis
                myToken = if (adminAccount == null) {
                    JWTFactory.sign(verifyCodeRequest.phoneNumber, false, verifyCodeRequest.templeId)
                } else {
                    JWTFactory.sign(verifyCodeRequest.phoneNumber, true, verifyCodeRequest.templeId)
                }

                //temp for repeat request to verify code, return previous token instead sign a new one.
                RedisManager.addKeyToRedis(
                    keyForTempToken,
                    Gson().toJson(myToken),
                    Constants.VERIFICATION_CODE_EXPIRE_TIME
                )

                RedisManager.addKeyToRedis(myToken.refreshToken, "aloha", Constants.REFRESH_TOKEN_EXPIRE_TIME_SECONDS)
            }

            if (myToken == null) {
                call.respond(
                    HttpStatusCode.InternalServerError
                )
            } else {
                call.respond(
                    HttpStatusCode.OK, mapOf(
                        TOKEN to myToken.token,
                        REFRESH_TOKEN to myToken.refreshToken,
                        TOKEN_TYPE to "bearer"
                    )
                )
            }
        } else {
            call.respond(HttpStatusCode.Unauthorized, BaseResponse("Validation Failed - no verify code."))
        }
    }
}

fun generateKeyForTempToken(templeId: String, phoneNumber: String): String {
    return StringBuilder().append(templeId)
        .append("=")
        .append(phoneNumber)
        .toString()
}
