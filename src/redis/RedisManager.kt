package com.authorization.redis

import redis.clients.jedis.Jedis

object RedisManager {
    val REDIS_EXPIRE_TIME = 600L

    // defualt settings
    var REDIS_HOST = "redis"
    var REDIS_PORT = 6379

    fun getValue(key: String) : String? {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        val value = jedis.get(key)
        jedis.close()
        return value
    }

    fun addKeyToRedis(key: String, value: String) {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        jedis.set(key, value)
        jedis.expire(key, REDIS_EXPIRE_TIME)
        jedis.close()
    }

    fun addKeyToRedis(key: String, value: String, expiredTime: Long) {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        jedis.set(key, value)
        jedis.expire(key, expiredTime)
        jedis.close()
    }

    fun addKeyToRedis(key: String) {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        jedis.set(key, "valid")
        jedis.expire(key, REDIS_EXPIRE_TIME)
        jedis.close()
    }

    fun hasKeyOnRedis(key: String): Boolean {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        val result = jedis.exists(key)
        jedis.close()
        return result
    }

    fun checkVerificationCode(phoneNumber: String, verificationCode: String): Boolean {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        val result = jedis.get(phoneNumber) == verificationCode
        jedis.close()
        return result
    }


    fun deleteByKey(verificationCode: String?): Long {
        val jedis = Jedis(REDIS_HOST, REDIS_PORT)
        verificationCode?.let { code ->
            return jedis.del(code)
        }
        jedis.close()
        return 0L
    }
}